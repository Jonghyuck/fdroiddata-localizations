#!/usr/bin/env python3

import glob
import os
import sys

try:
    from lxml import etree as ET
    print("running with lxml.etree")
except ImportError:
    import xml.etree.cElementTree as ET

errors = set()

def write_if_text_changed(basedir, filename, text):
    sourcedir = '/'.join(basedir.split('/')[:8] + ['en-US'])
    if not os.path.exists(sourcedir):
        errors.add('source removed: ' + os.path.dirname(basedir))
        return
    os.makedirs(basedir, exist_ok=True)
    path = os.path.join(basedir, filename)
    if os.path.exists(path):
        with open(path) as fp:
            contents = fp.read()
            # for LtR and RtL
            if contents.startswith(text.rstrip()) or contents.endswith(text.strip() + '\n'):
                print('unchanged, ignoring', path)
                return
    with open(path, 'w') as fp:
        fp.write(text.replace('\\n', '\n'))

metadatadir = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
                           'fdroiddata', 'metadata')

# take any title or summary, descriptions must be complete
for f in sorted(glob.glob('localizations/*.xliff')):
    locale = f[14:-6]
    print('add untranslated strings', locale + '\t' + f)
    if locale == 'en-US':
        continue

    parser = ET.XMLParser(remove_blank_text=True)
    tree = ET.parse(f, parser)
    treeroot = tree.getroot()
    body = treeroot.find('file').find('body')

    description = dict()
    for trans_unit in body.findall('trans-unit'):
        target = trans_unit.find('target')
        if target is None:
            continue

        if trans_unit.attrib['id'].endswith('-summary'):
            packageName = '-'.join(trans_unit.attrib['id'].split('-')[:-1])
            basedir = os.path.join(metadatadir, packageName, locale)
            print('\tfound:', trans_unit.attrib['id'], target.text)
            if target.text is not None:
                write_if_text_changed(basedir, 'summary.txt', target.text.strip() + '\n')
        elif trans_unit.attrib['id'].endswith('-name'):
            packageName = '-'.join(trans_unit.attrib['id'].split('-')[:-1])
            basedir = os.path.join(metadatadir, packageName, locale)
            print('\tfound:', trans_unit.attrib['id'], target.text)
            if target.text is not None:
                write_if_text_changed(basedir, 'name.txt', target.text.strip() + '\n')
        elif '-changelog-' in trans_unit.attrib['id']:
            trans_unit_id = trans_unit.attrib['id'].split('-')
            packageName = '-'.join(trans_unit_id[:-2])
            versionCode = trans_unit_id[-1]
            basedir = os.path.join(metadatadir,
                                   packageName, locale, 'changelogs')
            if target.text is not None:
                write_if_text_changed(basedir, versionCode + '.txt',
                                      target.text.strip().replace('\\n', '\n') + '\n')
        elif '-description-' in trans_unit.attrib['id']:
            print('including', locale, trans_unit.attrib['id'], type(target.text))
            packageName = '-'.join(trans_unit.attrib['id'].split('-')[:-2])
            if target.text is not None:
                if packageName not in description:
                    description[packageName] = ''
                description[packageName] += target.text.replace('\\n', '\n') + '\n\n'
    if description:
        for packageName, description in description.items():
            sourcedir = os.path.join(metadatadir, packageName, 'en-US')
            if not os.path.exists(sourcedir):
                errors.add('source removed: ' + os.path.dirname(sourcedir))
                continue
            print('write description:', packageName, locale)
            basedir = os.path.join(metadatadir, packageName, locale)
            os.makedirs(basedir, exist_ok=True)
            with open(os.path.join(basedir, 'description.txt'), 'w', encoding='utf-8') as fp:
                fp.write(description.strip() + '\n')

if errors:
    print('\nERRORS\n----------------------------------------------')
    for error in sorted(errors):
        print(error)
    if errors:
        print('\ncheck:')
        print('gitk --', ' '.join(['metadata/%s.yml' % f.split('/')[-1] for f in errors]))
        print('\nfix:')
        print('for f in', ' '.join([f.split('/')[-1] for f in errors]), '; do perl -i -pe "BEGIN{undef $/;} s/\\n *<trans-unit id=\\"${f}-.*?<\/trans-unit>\\n/\\n/smg" localizations/*.xliff; done')
    sys.exit(len(errors))
