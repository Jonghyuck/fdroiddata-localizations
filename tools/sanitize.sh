#!/bin/sh

sed -i \
    -e 's,</target>\s*<source>,</target>\n        <source>,g' \
    -e 's,</source>\s*<target>,</source>\n        <target>,g' \
    -e 's,>\s*</trans-unit>,>\n      </trans-unit>,g' \
    localizations/*.xliff


sed -i \
    -e 's,f-droid.org,f-droid.org,ig' \
    localizations/*.xliff
